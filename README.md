# Project Node-wot <-> Thingweb <-> GraphDB

## Installation process

### Install node-red :  

`$ sudo npm install -g --unsafe-perm node-red`

to launch it :  
`$ node-red`

### Launch GraphDB  

Get graphdb here (8.10.1 recommended) : `https://www.ontotext.com/products/graphdb/graphdb-free/`  

Go into the folder : `graphdb-free-8.10.1/bin`  
Then launch `$ ./graphdb -d`  

This command returns nothing. Wait a little bit.  

GraphDB interface will be accessible on `http://localhost:7200`  

### Launch thingweb directory

Clone thingweb : `$ git clone https://github.com/thingweb/thingweb-directory/tree/4641f260190805034ed11c11b0591280fca06d1b`  

First you have to set the environment variables to match thingweb and GraphDB.  
`export THINGWEB_SPARQL_QUERY_ENDPOINT=http://localhost:7200/repositories/<your_repository_name>`  
`export THINGWEB_SPARQL_UPDATE_ENDPOINT=http://localhost:7200/repositories/<your_repository_name>/statements`  

Go into the folder : `thingweb-directory/directory-app`  
Then launch : `$ gradle run`  

### Install iotschema-node-red  

Get iotschema-node-red at : `https://github.com/iot-schema-collab/iotschema-node-red`  

Go into local folder of node-red  

Linux : `$ cd ~/.node-red`  
Windows (Powershell) : `PS C:\Users\{username}\AppData\Roaming\npm\node_modules\node-red`

`$ npm i -g recursive-install`

`$ npm-recursive-install --rootDir={YOUR_OWN_PATH}/iotschema-node-red`  

## Get started !

### Node-wot side
Now that everything is setup we can use the two files in this repository.  
* The first one is the **ruleset** to use when you create a new repository in GraphDB (the file '.pie').
* The second one is an example to start with node-red.
  * Open this file in node-red and on the *thing-directory* tab, choose the *Use Remote TD Server* option then type `localhost:8080` (wich is the adress used for thingweb).
  * Then store the TD.
  * The recipe will be automatically redirected to GraphDB

### GraphDB side

From here you should be able to query the *KelvinTemperatureSensor* since it's an "iot:TemperatureSensor".  

With this query you should be able to find the *KelvinTemperatureSensor* **and** a fictive node called *CelsiusTemperatureSensor* (a fictive node is a node created with the inference ruleset).  
```SQL
SELECT * WHERE {
    ?s iot:capability "iot:TemperatureSensor".  
    ?s ?p ?o.
}
```  

For the tests, within the ruleset there is a device called "KelvinToCelsius".  
It allow the ruleset to create new nodes as soon as a *KelvinTemperatureSensor* is detected (i.e. A device that produce temperature observation result in Kelvin).  

It is also possible to modify the ruleset to add, in a second time the required convertor through **node-wot**.


# If there is any problem like : EACCESS

type : `$ sudo npm config set unsafe-perm=true`
